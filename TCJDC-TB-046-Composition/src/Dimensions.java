
public class Dimensions {

	private int width;
	private int height;
	private int depth;

	public Dimensions(int width, int height, int depth) {
		super();
		this.width = width;
		this.height = height;
		this.depth = depth;
	}

}
